#!/usr/bin/env sh

log_event() {
  msg="$(date +%Y-%m-%dT%H:%M:%S) | $1"
  echo "$msg" >>"$2"
  echo "$msg"
}

cd2loader() {
  file_dir="$(dirname "$1")"

  cd "${file_dir:-.}" || return 10

  return 0
}

check() {
  if ! command -v python3 1>/dev/null 2>/dev/null; then
    echo "ERROR: python3 not found"
    return 20
  elif ! python3 -m pip --version 1>/dev/null 2>/dev/null; then
    echo "ERROR: pip not found"
    return 21
  elif [ ! -d .git ]; then
    echo "ERROR: .git dir not found"
    return 22
  fi

  return 0
}

cron_set() {
  expr "${*}" : ".* skip-cron .*" 1>/dev/null 2>/dev/null && return

  if ! command -v crontab 1>/dev/null 2>/dev/null; then
    log_event "ERROR: cron not found" loader_launcher.log
    return 23
  fi

  echo "@reboot root sh '$PWD/${1##*/}'" |
    sudo tee "/etc/cron.d/loader_launcher"

  log_event "CRON:@reboot root sh '$PWD/${1##*/}'" loader_launcher.log

  return 0
}

run() {
  [ ! -e loader_launcher.log ] && touch loader_launcher.log
  [ ! -e loader_run.log ] && touch loader_run.log

  log_event "LAUNCHER START" loader_launcher.log

  export PIP_TARGET="$PWD/Modules"
  export PYTHONPATH="$PWD/Modules"

  python3 -m pip install --upgrade pip
  log_event "PIP UPDATE:$?" loader_launcher.log

  requirements_loader_curr=""
  requirements_app_curr=""
  requirements_loader_prev=""
  requirements_app_prev=""
  install_app_curr=""
  install_app_prev=""
  failed_runs=0
  failed_runs_max=5
  failed_runs_time=5
  app_use_default=0

  while true; do
    requirements_loader_curr="$(cat requirements.txt 2>/dev/null)"
    requirements_app_curr="$(cat App/requirements.txt 2>/dev/null)"
    install_app_curr="$(cat App/install.sh 2>/dev/null)"

    if [ -e requirements.txt ] && [ "$requirements_loader_prev" != "$requirements_loader_curr" ]; then
      requirements_loader_prev="$requirements_loader_curr"
      python3 -m pip install --upgrade -r requirements.txt
      pip_status="$?"
      log_event "PYPI REQUIREMENTS LOADER:$pip_status" loader_launcher.log
      [ $pip_status -ne 0 ] && requirements_loader_prev=""
    else
      log_event "PYPI REQUIREMENTS LOADER:NO CHANGES" loader_launcher.log
    fi
    if [ -e App/requirements.txt ] && [ "$requirements_app_prev" != "$requirements_app_curr" ]; then
      requirements_app_prev="$requirements_app_curr"
      python3 -m pip install --upgrade -r App/requirements.txt
      pip_status="$?"
      log_event "PYPI REQUIREMENTS APP:$pip_status" loader_launcher.log
      [ $pip_status -ne 0 ] && requirements_app_prev=""
    else
      log_event "PYPI REQUIREMENTS APP:NO CHANGES" loader_launcher.log
    fi
    if [ -e App/install.sh ] && [ "$install_app_prev" != "$install_app_curr" ]; then
      sudo sh App/install.sh
      install_status="$?"
      log_event "INSTALL APP:$install_status" loader_launcher.log
    else
      log_event "INSTALL APP:NO CHANGES" loader_launcher.log
    fi

    log_event "LOADER START" loader_launcher.log

    loader_time_start="$(date +%s)"
    if [ $app_use_default -eq 0 ]; then
      python3 loader.py >>loader_run.log 2>&1
      return_code=$?
    else
      log_event "LOADER START:USE DEFAULT APP" loader_launcher.log
      python3 loader.py --use-default-app >>loader_run.log 2>&1
      return_code=$?
    fi
    loader_time_end="$(date +%s)"

    log_event "LOADER EXIT ${return_code}" loader_launcher.log
    log_event "LOADER RUN TIME:$((loader_time_end - loader_time_start))" loader_launcher.log

    if test "$((loader_time_end - loader_time_start))" -le $failed_runs_time; then
      failed_runs="$((failed_runs + 1))"
      log_event "FAILED RUNS:$failed_runs" loader_launcher.log
    else
      failed_runs=0
    fi

    if [ $return_code = 130 ]; then
      break
    elif [ $failed_runs -ge $failed_runs_max ]; then
      if [ $app_use_default -eq 0 ]; then
        log_event "FAILED RUNS:USE DEFAULT APP" loader_launcher.log
        failed_runs=0
        app_use_default=1
      else
        log_event "FAILED RUNS:EXIT" loader_launcher.log
        break
      fi
    fi
  done

  log_event "RUN EXIT" loader_launcher.log

  return 0
}

main() {
  cd2loader "$@" || return $?
  check "$@" || return $?
  cron_set "$@" || return $?
  expr "${*}" : ".* set-cron .*" 1>/dev/null 2>/dev/null && {
    log_event "LAUNCHER:CRON SET" loader_launcher.log
    return 0
  }
  run || return $(($? + 30))
}

main "$0" "$@" " "
ret="$?"
log_event "LAUNCHER EXIT:$ret" loader_launcher.log
exit $ret
