from os import chdir
from os import path
from sys import argv

from Loader import loader

if __name__ == "__main__":
    work_dir: str = path.dirname(path.abspath(__file__))

    chdir(work_dir)

    loader(work_dir, app_use_default="--use-default-app" in argv[1:])
