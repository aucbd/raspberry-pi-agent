from json import dumps as json_dumps
from json import loads as json_loads
from os import system
from os.path import isdir
from shutil import rmtree
from time import perf_counter
from time import sleep
from time import time
from typing import Any
from typing import Callable
from typing import Dict
from typing import Optional
from typing import Union

from .Agent import Agent
from .Agent import MQTTMessage
from .Server import server_close
from .Server import server_start
from .Update import update
from .Variables import Variables


def loader_kill(exit_code: int = 0):
    server_close()
    Variables.log.write("KILL")
    Variables.settings.save()
    raise SystemExit(exit_code)


def loader_restart(ag: Agent):
    Variables.log.write("LOADER RESTART")
    ag.publish("console", "LOADER RESTART")
    ag.loop(2)
    sleep(2)
    loader_kill()


def loader_close(ag: Agent):
    Variables.log.write("LOADER CLOSE")
    ag.publish("console", "LOADER CLOSE")
    ag.loop(2)
    sleep(2)
    loader_kill(130)


def reboot(ag: Agent):
    Variables.log.write("SYSTEM REBOOT")
    ag.publish("console", "SYSTEM REBOOT")
    ag.loop(2)
    sleep(2)
    system("sudo reboot")


def remove_app(ag: Agent):
    rmtree(Variables.settings["folders"]["app"])
    Variables.log.write("APP REMOVED")
    ag.publish("console", "APP REMOVED")
    ag.loop(1)


def callback_disconnect(ag: Agent, _, __):
    Variables.log.write(f"DISCONNECTED")

    ag.connect(host=Variables.settings["mqtt"]["host"], port=Variables.settings["mqtt"].get("port", 1883))

    t: float = perf_counter() + Variables.settings["mqtt"].get("connection_timeout", 30)
    while not ag.is_connected() and perf_counter() < t:
        ag.loop(1)
        sleep(0.5)

    if not ag.is_connected():
        Variables.log.write("RECONNECTION ERROR")
        loader_kill(1)


def callback_control(ag: Agent, __, msg: MQTTMessage):
    Variables.log.write(f"CONTROL:{msg.payload.decode()}")
    reply_log: Any = None

    command: Optional[str] = None
    token: Optional[str] = None
    reply: Optional[Union[int, float, str, dict, list, tuple]] = None
    exec_time: int = int(time())
    success: int = 1
    post_func: Optional[Callable] = None

    try:
        msg_json: Dict[str, Any] = json_loads(msg.payload.decode())
        command = msg_json.get("command", None)
        token = msg_json.get("token", None)

        if token:
            ag.publish("response", json_dumps({"token": token}))
            ag.loop(0.1)

        if not command:
            success = 0
            reply = "NO COMMAND"
        elif command == "status":
            Variables.settings.load()
            reply = {
                "starttime": Variables.settings["start_time"],
                "runtime": time() - Variables.settings["start_time"],
                "platform": dict(Variables.settings["platform"]),
                "version": dict(Variables.settings["version"])
            }
        elif command == "log":
            reply = "\n".join(Variables.log.read(msg_json.get('value', 1)))
        elif command == "setting":
            Variables.settings.load()
            Variables.settings.set(msg_json["value"], *msg_json["key"].split("."))
            Variables.settings.save()
        elif command == "read":
            Variables.settings.load()
            key = msg_json.get("key", "")
            reply = Variables.settings.read(*key.split(".")) if key else dict(Variables.settings)
        elif command == "runtime":
            reply = round(time() - Variables.settings["start_time"], 3)
        elif command == "delete":
            Variables.settings.load()
            Variables.settings.delete(*msg_json["key"].split("."))
            Variables.settings.save()
        elif command == "server_start":
            server_host = msg_json.get("host", "0.0.0.0")
            server_port = msg_json.get("port", 8080)
            server_status: bool = server_start(host=server_host, port=server_port)
            if server_status is True:
                reply = f"SERVER:START {server_host}:{server_port}"
            else:
                success = 0
                reply_log = reply = f"SERVER:START ERROR"
        elif command == "server_close":
            server_close()
            reply = f"SERVER:CLOSE"
        elif command == "restart":
            post_func = (lambda: loader_restart(ag))
        elif command == "close":
            post_func = (lambda: loader_close(ag))
        elif command == "reboot":
            post_func = (lambda: reboot(ag))
        elif command == "remove_app":
            if isdir(Variables.settings["folders"]["app"]):
                remove_app(ag)
                post_func = (lambda: loader_restart(ag))
            else:
                reply = "REMOVE APP:NO APP"
        elif command == "update":
            if not ("loader" in msg_json or "app_remote" in msg_json or "app_version" in msg_json):
                msg_json["loader"] = "HEAD"
                if Variables.settings["version"]["app"]:
                    msg_json["app_version"] = "HEAD"

            target_loader: Optional[str] = msg_json.get("loader", None)
            target_app_remote: Optional[str] = msg_json.get("app_remote", None)
            target_app_version: Optional[str] = msg_json.get("app_version", None)
            update(target_loader, target_app_remote, target_app_version)
            post_func = (lambda: loader_restart(ag))
        else:
            success = 0
            reply_log = reply = f"UNKNOWN COMMAND:{command}"
    except (BaseException, Exception) as control_err:
        success = 0
        reply = reply_log = repr(control_err)

    ag.publish("response", json_dumps({
        "token": token,
        "command": command,
        "time": exec_time,
        "success": success,
        "reply": reply
    }))

    if reply_log is not None:
        Variables.log.write(str(reply_log))

    if post_func is not None:
        post_func()
