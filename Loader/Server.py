from json import dumps
from json import loads
from multiprocessing import Process

from flask import Flask
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for

from .Variables import Variables

server: Flask = Flask(__name__)


def server_close():
    if Variables.server_process is not None:
        if Variables.server_process.is_alive():
            Variables.server_process.terminate()
            Variables.server_process.join()
        Variables.server_process = None
        Variables.log.write(f"SERVER: CLOSE")


def server_start(host: str = "0.0.0.0", port: int = 8080) -> bool:
    server_close()
    Variables.server_process = Process(
        target=server.run,
        args=(host, port,)
    )
    Variables.server_process.start()
    Variables.log.write(f"SERVER: START {host}:{port}")

    return Variables.server_process.is_alive()


@server.route("/")
def home():
    return redirect(url_for("settings_edit"))


@server.route("/settings/")
def settings_edit():
    Variables.settings.load()
    settings: dict = Variables.settings.flatten()
    settings = {k: dumps(v) if isinstance(v, str) else v for k, v in settings.items()}

    return render_template(
        "settings.html",
        settings=settings
    )


@server.route("/settings_write/")
def settings_write():
    Variables.settings.load()
    old_settings: dict = Variables.settings.flatten()
    new_settings: dict

    if request.args.get("settings", None) is not None:
        new_settings = loads(request.args.get("settings"))
    else:
        new_settings = old_settings

    add_settings: dict = {
        k: v
        for k, v in new_settings.items()
        if k not in old_settings or old_settings[k] != v
    }
    rem_settings: list = [
        k
        for k in old_settings
        if k not in new_settings
    ]

    for k, v in add_settings.items():
        Variables.log.write(f"SETTING[{k}]:{v}")
        Variables.settings.set(v, *k.split("."))
    for k in rem_settings:
        Variables.log.write(f"DELETE[{k}]")
        Variables.settings.delete(*k.split("."))

    Variables.settings.save()

    return render_template(
        "settings_written.html",
        settings={k: dumps(v) if isinstance(v, str) else v for k, v in new_settings.items()}
    )
