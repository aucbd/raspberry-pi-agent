from json import dumps as json_dumps
from typing import Any
from typing import Callable
from typing import List
from typing import Tuple
from typing import Union

import paho.mqtt as mqtt
import paho.mqtt.client as client


class MQTTException(mqtt.MQTTException):
    pass


class MQTTMessage(client.MQTTMessage):
    pass


class Agent(client.Client):
    def __init__(self, project: str, name: str, client_id: str = "", clean_session=None, userdata=None,
                 protocol: int = client.MQTTv311, transport: str = "tcp"):
        super().__init__(client_id, clean_session, userdata, protocol, transport)
        self.project: str = project
        self.name: str = name
        self.subscribed_topics: List[str] = []
        self.callback_topics: List[str] = []

    def publish(self, topic: str, payload: Union[str, bytes] = "", qos: int = 0, retain: bool = False, properties=None
                ) -> client.MQTTMessageInfo:
        return super().publish("/".join([self.project, self.name, topic]), payload, qos, retain, properties)

    def publish_stream(self, topic: str, payload: Any = "", qos: int = 0, retain: bool = False, properties=None
                       ) -> client.MQTTMessageInfo:
        payload_json: dict = {"value": payload}
        return super().publish("/".join([self.project, self.name, topic]), json_dumps(payload_json), qos, retain,
                               properties)

    def publish_raw(self, topic: str, payload: Union[str, bytes] = "", qos: int = 0, retain: bool = False,
                    properties=None) -> client.MQTTMessageInfo:
        return super().publish(topic, payload, qos, retain, properties)

    def subscribe(self, topic: str, qos: int = 0, options=None, properties=None
                  ) -> Union[Tuple[int, None], Tuple[Union[int, Any], int]]:
        return self.subscribe_raw("/".join([self.project, self.name, topic]), qos, options, properties)

    def subscribe_raw(self, topic: str, qos: int = 0, options=None, properties=None
                      ) -> Union[Tuple[int, None], Tuple[Union[int, Any], int]]:
        self.subscribed_topics.append(topic) if topic not in self.subscribed_topics else None
        return super().subscribe(topic, qos, options, properties)

    def unsubscribe(self, topic: str, properties=None) -> Union[Tuple[int, None], Tuple[Union[int, Any], int]]:
        return self.unsubscribe_raw("/".join([self.project, self.name, topic]), properties)

    def unsubscribe_raw(self, topic: str, properties=None) -> Union[Tuple[int, None], Tuple[Union[int, Any], int]]:
        self.subscribed_topics.remove(topic) if topic in self.subscribed_topics else None
        return super().unsubscribe(topic, properties)

    def message_callback_add(self, sub: str, callback: Callable[['Agent', Any, MQTTMessage], None]) -> Any:
        return self.message_callback_add_raw("/".join([self.project, self.name, sub]), callback)

    def message_callback_add_raw(self, sub: str, callback: Callable[['Agent', Any, MQTTMessage], None]) -> Any:
        self.callback_topics.append(sub) if sub not in self.callback_topics else None
        return super().message_callback_add(sub, callback)

    def message_callback_remove(self, sub: str) -> Any:
        return self.message_callback_remove_raw("/".join([self.project, self.name, sub]))

    def message_callback_remove_raw(self, sub: str) -> Any:
        self.callback_topics.remove(sub) if sub in self.callback_topics else None
        return super().message_callback_remove(sub)
