from multiprocessing import Process

from .Log import Log
from .Settings import Settings


class Variables:
    log: Log = None
    settings: Settings = None
    server_process: Process = None
