from datetime import datetime
from typing import List


class Log:
    def __init__(self, logfile: str):
        self.logfile: str = logfile
        open(self.logfile, "a").close()

    def read(self, lines: int = 1) -> List[str]:
        with open(self.logfile, "r") as f:
            return [line.strip() for line in f.readlines()[-lines:]]

    def write(self, data: str, print_log: bool = True):
        data = f"{datetime.now().isoformat()} | {data}\n"
        with open(self.logfile, "a") as f:
            f.seek(0, 2)
            f.write(data)
        if print_log:
            print(data.strip())
