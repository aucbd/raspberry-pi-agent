from .Agent import Agent
from .Log import Log
from .Settings import Settings


def app(_settings: Settings, _log_arg: Log, agent: Agent):
    while True:
        agent.loop(1)
