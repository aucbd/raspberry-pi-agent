from collections import MutableMapping
from collections import UserDict
from copy import deepcopy
from functools import reduce
from json import dumps as json_dumps
from json import load as json_load
from json import loads as json_loads
from operator import getitem
from os.path import isfile
from typing import Any
from typing import Union

from deep_merge import merge


def flatten(d: MutableMapping, parent_key: str = "", sep: str = ".") -> dict:
    items = []
    for k, v in d.items():
        k_new = parent_key + sep + k if parent_key else k
        if isinstance(v, MutableMapping) and len(v):
            items.extend(flatten(v, k_new).items())
        else:
            items.append((k_new, v))

    return dict(items)


class Settings(UserDict):
    def __init__(self, settings_file: str):
        self.settings_file: str = settings_file
        super().__init__()
        self.load()

    def load(self):
        if not isfile(self.settings_file):
            self.save()
            return

        backup: Settings = deepcopy(self)
        self.clear()

        try:
            with open(self.settings_file, "r") as f:
                for k, v in json_load(f).items():
                    self[k] = v
        except (BaseException, Exception) as err:
            self.clear()
            for k, v in backup.items():
                self[k] = v
            raise err

    def save(self):
        settings_str: str = json_dumps(dict(self), indent=2)
        with open(self.settings_file, "w") as f:
            f.write(settings_str)

    def merge(self, other: Union['Settings', dict], overwrite: bool = True):
        if overwrite:
            temp = deepcopy(dict(self))
            merge(temp, dict(other))
        else:
            temp = deepcopy(dict(other))
            merge(temp, dict(self))
        for k, v in temp.items():
            self[k] = deepcopy(v)

    def read(self, *keys: Any) -> Any:
        return reduce(getitem, keys, self)

    def set(self, value: Any, *keys: Any):
        setting = self
        for key in keys[:-1]:
            setting = setting.setdefault(key, {})
        setting[keys[-1]] = json_loads(json_dumps(value))
        self.save()

    def delete(self, *keys: Any):
        setting = self
        for key in keys[:-1]:
            setting = setting[key]
        del setting[keys[-1]]
        self.save()

    def flatten(self) -> dict:
        return flatten(self)
