from copy import copy
from os import path
from sys import exit
from time import perf_counter
from time import sleep
from time import time
from typing import Any
from typing import Callable
from typing import List
from typing import Optional
from typing import Tuple

from git import InvalidGitRepositoryError
from git import Repo

from .Agent import Agent
from .Agent import MQTTException
from .Callbacks import callback_control
from .Callbacks import callback_disconnect
from .Callbacks import loader_restart
from .Callbacks import remove_app
from .Log import Log
from .Serial import get_serial
from .Serial import get_uuid_from_serial
from .Serial import platform
from .Settings import Settings
from .Variables import Variables


def import_app(use_default: bool = False) -> Tuple[Callable[[Settings, Log, Agent], Any], str, str]:
    if use_default:
        Variables.log.write(f"APP IMPORT:USE DEFAULT")
        from .AppDefault import app

        return app, "", ""

    try:
        import App
        app_repo = Repo(Variables.settings["folders"]["app"])

        return App.app, next(app_repo.remote("origin").urls), app_repo.commit("HEAD").hexsha[:7]
    except (ModuleNotFoundError, ImportError, InvalidGitRepositoryError) as import_err:
        Variables.log.write(f"APP IMPORT ERROR:{repr(import_err)}")
        from .AppDefault import app

        return app, "", ""
    except (BaseException, Exception) as err:
        Variables.log.write(f"APP IMPORT UNKNOWN ERROR:{repr(err)}")
        from .AppDefault import app

        return app, "", ""


def loader(work_dir: str, app_use_default: bool = False):
    Variables.log = Log("main.log")
    Variables.settings = Settings(path.join(work_dir, "settings.json"))

    agent: Optional[Agent] = None
    return_code: int = 0

    Variables.log.write("LOADER START")

    try:
        app, app_remote, app_version = import_app(use_default=app_use_default)
        serial_platform = get_serial()

        Variables.settings["start_time"] = time()
        Variables.settings["platform"] = {
            "platform": platform,
            "serial_platform": serial_platform,
            "serial": get_uuid_from_serial(serial_platform, 22),
        }
        Variables.settings["version"] = {
            "app": app_version,
            "app_remote": app_remote,
            "loader": Repo(work_dir).commit("HEAD").hexsha[:7],
        }
        Variables.settings["folders"] = Variables.settings.get("folders", {})
        Variables.settings["folders"] = {
            "loader": path.join(work_dir, "Loader"),
            "app": path.join(work_dir, "App"),
            "data": Variables.settings["folders"].get("data", path.join(work_dir, "data")),
            "root": work_dir,
        }

        if path.isfile(path.join(Variables.settings["folders"]["app"], "settings.json")):
            settings_app: Settings = Settings(
                path.join(Variables.settings["folders"]["app"], "settings.json"))
            if settings_app.get("version", {}).get("loader", None) is not None:
                del settings_app["version"]["loader"]

            Variables.settings.merge(settings_app, overwrite=False)

        Variables.settings.save()

        Variables.log.write(f"VERSION LOADER:{Variables.settings['version']['loader']}")
        Variables.log.write(f"VERSION APP REMOTE:{Variables.settings['version']['app_remote'] or None}")
        Variables.log.write(f"VERSION APP VERSION:{Variables.settings['version']['app'] or None}")

        agent = Agent(
            project=Variables.settings["mqtt"]["project"],
            name=Variables.settings["mqtt"]["name"],
            client_id=Variables.settings["mqtt"].get("client_id", ""),
        )

        agent.username_pw_set(Variables.settings["mqtt"]["username"], Variables.settings["mqtt"]["password"])
        agent.on_connect = lambda cl, __, ___, rc: (Variables.log.write(f"CONNECTION:{rc}"), cl.subscribe("$SYS/#"),)
        agent.on_disconnect = callback_disconnect
        agent.connect(host=Variables.settings["mqtt"]["host"], port=Variables.settings["mqtt"].get("port", 1883))

        agent.loop(1)

        app_time: float = perf_counter() + Variables.settings["mqtt"].get("connection_timeout", 30)
        while not agent.is_connected() and perf_counter() < app_time:
            agent.loop(1)
            sleep(0.5)

        if not agent.is_connected():
            raise MQTTException("Connection timed out")
        else:
            agent.subscribe("control")
            agent.message_callback_add("control", callback_control)

            agent.subscribe("ping")
            agent.message_callback_add("ping", lambda ag, *_: ag.publish("console", "ONLINE"))

            agent.publish("console", "LOADER START")
            agent.publish("console", f"VERSION LOADER:{Variables.settings['version']['loader']}")
            agent.publish("console", f"VERSION APP REMOTE:{Variables.settings['version']['app_remote'] or None}")
            agent.publish("console", f"VERSION APP VERSION:{Variables.settings['version']['app'] or None}")

            app_fail: int = 0
            base_topics: List[str] = copy(agent.subscribed_topics)
            base_callbacks: List[str] = copy(agent.callback_topics)

            while True:
                app_time: float = perf_counter()
                try:
                    if app_version:
                        Variables.log.write("APP START")
                        agent.publish("console", "APP START")
                    agent.loop(1)
                    app(Variables.settings, Variables.log, agent)
                    app_fail = 0
                except KeyboardInterrupt:
                    raise
                except SystemExit:
                    raise
                except (BaseException, Exception) as app_err:
                    app_time = perf_counter() - app_time
                    app_fail += 1 if app_time < 5 else 0
                    Variables.log.write(f"APP ERROR:{repr(app_err)}")
                    agent.publish("console", f"APP ERROR:{repr(app_err)}")
                    if app_fail:
                        Variables.log.write(f"APP ERROR:FAILED RUNS {app_fail}")
                        agent.publish("console", f"APP ERROR:FAILED RUNS {app_fail}")
                    if app_fail >= 5:
                        remove_app(agent)
                        loader_restart(agent)
                finally:
                    for topic in agent.subscribed_topics:
                        agent.unsubscribe_raw(topic) if topic not in base_topics else None
                    for topic in agent.callback_topics:
                        agent.message_callback_remove_raw(topic) if topic not in base_callbacks else None
    except SystemExit as exit_code:
        Variables.log.write(f"MANUAL EXIT:{exit_code.code}")
        return_code = exit_code.code
    except KeyboardInterrupt:
        Variables.log.write("MANUAL INTERRUPT")
        return_code = 130
    except (BaseException, Exception) as err:
        Variables.log.write(f"FATAL ERROR:{repr(err)}")
        return_code = 1
    finally:
        if isinstance(agent, Agent):
            agent.on_disconnect = None
            agent.disconnect()
            while agent.is_connected():
                sleep(0.5)
        Variables.log.write(f"LOADER EXIT")
        exit(return_code)
