from os import popen
from re import search
from sys import platform
from typing import List


def get_serial() -> str:
    serial: str = ""

    if "linux" in platform:
        serial_raw: str = popen("cat /proc/cpuinfo | grep -i '^serial'").read().strip()
        serial = search(r":[^\w]*([\dabcdefABCDEF]+)", serial_raw).group(1)
    elif "darwin" in platform:
        serial_raw: str = popen("ioreg -l | grep IOPlatformSerialNumber").read().strip()
        serial = str(search(r'=[^"]*"([\dA-Z]+)"', serial_raw).group(1))
    elif "win" in platform:
        serial_raw: str = popen("wmic bios get serialnumber").read().strip()
        serial = str(search(r'\s([\dA-Z]+)', serial_raw).group(1))

    return serial


def get_uuid_from_serial(serial: str = get_serial(), length: int = 22) -> str:
    serial_unique: List[int] = [c + ord(serial[c % len(serial)]) for c in range(length)]
    return "".join(map(lambda c: f"{c:02X}", serial_unique))
