from os import path
from re import match
from shutil import move
from shutil import rmtree
from typing import Optional
from typing import Tuple

from git import GitCommandError
from git import Repo

from .Variables import Variables


class UpdateError(Exception):
    pass


def check_update_loader(target_version: Optional[str]) -> bool:
    if not target_version:
        return True

    loader_repo: Repo = Repo(Variables.settings["folders"]["root"])
    loader_version: str = loader_repo.commit("HEAD").hexsha[:len(target_version)]
    target_version = target_version if target_version else "HEAD"

    if loader_version == target_version:
        return True
    elif match(r"^HEAD(~+|~\d+)?$", target_version):
        return True
    else:
        try:
            new_repo = Repo.clone_from(
                next(loader_repo.remote("origin").urls),
                path.join(Variables.settings["folders"]["root"], "temp", "loader")
            )
            new_repo.git.reset("--hard", target_version)
            return True
        except GitCommandError:
            return False


def check_update_app(target_remote: Optional[str], target_version: Optional[str]) -> Tuple[bool, str]:
    if not target_remote and not target_version:
        return True, ""

    target_version = target_version if target_version else "HEAD"

    if path.isdir(Variables.settings["folders"]["app"]):
        app_repo: Repo = Repo(Variables.settings["folders"]["app"])
        app_remote: str = next(app_repo.remote("origin").urls)
        app_version: str = app_repo.commit("HEAD").hexsha[:len(target_version)]
        target_remote = app_remote if not target_remote else target_remote

        if app_remote == target_remote and app_version == target_version:
            return True, ""

    try:
        new_repo_path: str = path.join(Variables.settings["folders"]["root"], "temp", "app")
        new_repo: Repo = Repo.clone_from(target_remote, new_repo_path)
        new_repo.git.reset("--hard", target_version)
        return True, new_repo_path
    except GitCommandError:
        return False, ""


def update_loader(target_version: str):
    loader_repo: Repo = Repo(Variables.settings["folders"]["root"])
    loader_repo.git.pull()
    loader_repo.git.reset("--hard", target_version)


def update_app(new_repo: str):
    if path.isdir(path.join(Variables.settings["folders"]["app"])):
        rmtree(path.join(Variables.settings["folders"]["app"]))
    move(new_repo, path.join(Variables.settings["folders"]["app"]))


def update(target_loader: Optional[str], target_app_remote: Optional[str], target_app_version: Optional[str]):
    try:
        loader_ok = check_update_loader(target_loader)
        app_ok, app_repo = check_update_app(target_app_remote, target_app_version)

        if not loader_ok:
            raise UpdateError("Invalid loader version")
        elif not app_ok:
            raise UpdateError("Invalid app remote or version")

        if target_loader:
            update_loader(target_loader)
        if app_repo:
            update_app(app_repo)
    finally:
        if path.isdir(path.join(Variables.settings["folders"]["root"], "temp")):
            rmtree(path.join(Variables.settings["folders"]["root"], "temp"))
