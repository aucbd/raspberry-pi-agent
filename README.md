# Raspberry Pi Agent

## Requirements

* Raspberry Pi (or other machine running Linux)
* Python 3.5 or above
* Python pip (bundled with Python)
* ZShell
* git (usually included with Unix distributions)
* cron (usually included with Unix distributions)

Python module requirements are handled by the launcher script automatically, provided pip is available.

## First Time Setup

Once the machine is ready, clone this repository into a folder of your choice, the scripts are location-agnostic.

**IMPORTANT:** the repository must be cloned, a copy will not work as the script uses git commands for updates and versioning.

A JSON settings file is needed for the script to run. This must be located in the repository folder and named `settings.json`.

The loader script only needs MQTT details to run, the following is an example file.

```json
{
  "mqtt": {
    "project": "project",
    "name": "name",
    "host": "your.platform",
    "username": "username",
    "password": "password",
    "connection_timeout": 10
  }
}
```

**NOTE:** `connection_timeout` is optional and defaults to 30 seconds.

Run `zsh loader_launcher.zsh`. The script will create a cron job at `/etc/cron.d/agent_launcher` and enable the cron service and reboot if the job was added for the first time.

Once the system reboots, the launcher will run automatically and start the loader script.

## Remote Control

The loader allows remote control via messages sent to the topic `[project]/[name]/control` in the following JSON format: `{"command": "[command]", "key": "[key]", "value": [value]}`. The reply will be sent to `[project]/[name]/console`.

The `key` and `value` fields are not needed for every command. For nested keys use `"key1.key2.key3"`.

Accepted commands:

* `read` Read setting located under `key`. The command can be used to read both values and objects, omitting `key` will return the whole settings file.
* `setting` Write `value` under `key`.
* `log` Return `value` lines from the log, omitting `value` will return only the last line of the log.
* `restart` Restart the loader.
* `reboot` Reboot the machine. Requires the user to be able to run `sudo reboot` without entering a password. Run `launcher_loader.zsh` as root to avoid the problem.
* `remove_app` Remove current app if present.

## Updates

Update instructions are sent to the `[project]/[name]/update` topic int he following JSON format: `{"loader": "[loader reference]", "app_remote": "[app git remote]", "app_version": "[app git reference]"}`. The loader and app references need to be git references (i.e. `HEAD` or commit hash). Keys can be omitted to avoid updating the loader or app. The `app_remote` value can be omitted if an app is already present on the machine, in that case the remote will be taken from the local app repository.

## Interface Classes

### Settings

The `Settings` class is used to handle the settings of the loader and app. It is based on the `collections.UserDict` to provide a dictionary-like interface with five extra methods.

* `Settings.save()` Save the current settings to `settings.json` (located in the loader folder).
* `Settings.load()` Load settings from `settings.json` (located in the loader folder)
* `Settings.merge(other: Union['Settings', dict])` Merge two instances of Settings (also allows to pass a dictionary).
* `Settings.read(*keys: Any) -> Any` Read a value from the settings given a list of keys.
* `Settings.set(value: Any, *keys: Any)` Write a value to the settings given a list of keys.

Settings are automatically saved when the loader exists, but in case of uncaught errors or premature terminations it is better to run `save()` after a value has been changed.

During startup, the loader populates the settings with version information and folder paths, these can be accessed by custom apps to save data and and to operate in the locations used by the loader.

The following is an example of the version and folders settings:

```json
{
  "version": {
    "app": "6212731",
    "app_remote": "https://bitbucket.org/aucbd/raspberry-pi-agent-app-camera",
    "loader": "d5364d2"
  },
  "folders": {
    "loader": "/home/pi/raspberry-pi-agent/Loader",
    "app": "/home/pi/raspberry-pi-agent/App",
    "data": "/home/pi/raspberry-pi-agent/data",
    "root": "/home/pi/raspberry-pi-agent"
  }
}
```

### Log

The `Log` class is used to write log entries to a `main.log` file located in the loader folder.

Log entries are saved in the format: `YYYY-MM-DDHH:MM:SS.SSSSSS | [message]`.

Two methods are available in the `Log` class:

* `Log.write(data: str, print_log: bool = True)` Write a new log entry and print it to screen by default unless `print_log` is explicitly set to `False`.
* `Log.read(lines: int = 1) -> List[str]` Return a list of the last `lines` lines from the log, by default it returns 1 line.

### Agent

The `Agent` class is used to handle MQTT connection and message processing. It is based on the client provided by the `paho-mqtt` module.

When initialised, it saves the project and name values taken from settings and uses them as root topics for all operations: `[project]/[name]`. For example `Agent.publish(topic, message)` will send `message` to `[project]/[name]/[topic]`.

The same methods as the original client are available, modified to use the topic root. The following is a short list of example commands:

```python
# Publish CONNECTED to [project]/[name]/console
agent.publish("console", "CONNECTED")

# Subscribe to [project]/[name]/control
agent.subscribe("control")

# Add function as callback for all [project]/[name]/control messages
agent.message_callback_add("control", function)
```

## Apps

Apps for the loader need to be formatted as Python modules that export an `app()` function that takes three arguments: `settings: Settings`, `log: Log` and `agent: Agent`.

A separate `settings.json` file can be included in the app and its content will be merged with the settings already on the machine.

The app is automatically imported and run by the loader script after connection has been established. If no app is present or the import fails then a default dummy app is run instead.

The following is an example app structure:

```
App/
├── __init__.py
├── app.py
└── settings.json
```

`__init__.py` would contain:

```python
from .app import app
```

`app.py` would contain:

```python
def app(settings: 'Settings', log_arg: 'Log', agent: 'Agent'):
    # App code here
```

Apps **must** run `Agent.loop()`, the loader does not run it in the background.
